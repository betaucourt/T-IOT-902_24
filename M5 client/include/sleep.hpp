#ifndef SLEEP_HPP
#define SLEEP_HPP

#include <M5Stack.h>

class Sleep {
  public:
    void init();
    void loop();
};

#endif