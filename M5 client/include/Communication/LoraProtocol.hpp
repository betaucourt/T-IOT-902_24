#ifndef LORA_HPP
#define LORA_HPP

#include <M5Stack.h>
#include <M5LoRa.h>
#include <string.h>
#include "ICommunication.hpp"

namespace M5Client
{
  namespace Communication
  {
    class LoraProtocol : public ICommunication
    {
    public:
      LoraProtocol();
      ~LoraProtocol();
      void Init();
      char *PostData(const char* data);
      void loopLoRa(const char* data);
      void Reinit();
    private:
      float frequency;
      LoRaClass lora;
    };
  }
}

#endif