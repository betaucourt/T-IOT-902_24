#ifndef ICOMMUNICATION_HPP
#define ICOMMUNICATION_HPP

namespace M5Client
{
  namespace Communication
  {
    class ICommunication
    {
    public:
      virtual void Init() = 0;
      virtual char *PostData(const char* data) = 0;
    };
  }
}

#endif