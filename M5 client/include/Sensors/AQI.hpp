#ifndef AQI_HPP
#define AQI_HPP

#include "DFRobot_SHT20.h"
#include <M5Stack.h>
#include <PMserial.h>

namespace M5Client
{
  namespace Sensors
  {
    enum sensorTypes {
      PM25 = 2,
      PM10 = 3,
      INTERVAL = 4
    };

    class AQI
    {
     public:
        AQI(const byte rxPin = 16, const byte txPin = 17);
        ~AQI();
        void AqiInit();
        void AqLoop();
        void handleError(const SerialPM &pms);
        const char *getFormattedAQI();
    private:
        const byte _rxPin;
        const byte _txPin;
        SerialPM *_pms;
        DFRobot_SHT20 _sht20;
        int _pm25;
        int _pm10;
        float _hum;
        float _temp;
        char *uuid = "grp25";
    };
  }
}

#endif