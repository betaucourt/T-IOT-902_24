#include "../include/sleep.hpp"

void Sleep::init() {
  M5.begin();
  Wire.begin();
  M5.Power.begin();
}

void Sleep::loop() {
  M5.Power.getBatteryLevel();
  M5.update();
  M5.Power.lightSleep(SLEEP_SEC(300));
}