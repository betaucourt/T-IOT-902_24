#include "Sensors/AQI.hpp"

M5Client::Sensors::AQI::AQI (const byte rxPin, const byte txPin) : _rxPin(rxPin), _txPin(txPin){

}
M5Client::Sensors::AQI::~AQI() {}

void M5Client::Sensors::AQI::AqiInit() {
     _pms = new SerialPM(PMS5003, _rxPin, _txPin);
    _pms->init();
    _sht20.initSHT20();
}

void M5Client::Sensors::AQI::handleError(const SerialPM &pms) {
    switch (pms.status) {
    case pms.OK: // should never come here
      break;     // included to compile without warnings
    case pms.ERROR_TIMEOUT:
      Serial.println(F(PMS_ERROR_TIMEOUT));
      break;
    case pms.ERROR_MSG_UNKNOWN:
      Serial.println(F(PMS_ERROR_MSG_UNKNOWN));
      break;
    case pms.ERROR_MSG_HEADER:
      Serial.println(F(PMS_ERROR_MSG_HEADER));
      break;
    case pms.ERROR_MSG_BODY:
      Serial.println(F(PMS_ERROR_MSG_BODY));
      break;
    case pms.ERROR_MSG_START:
      Serial.println(F(PMS_ERROR_MSG_START));
      break;
    case pms.ERROR_MSG_LENGTH:
      Serial.println(F(PMS_ERROR_MSG_LENGTH));
      break;
    case pms.ERROR_MSG_CKSUM:
      Serial.println(F(PMS_ERROR_MSG_CKSUM));
      break;
    case pms.ERROR_PMS_TYPE:
      Serial.println(F(PMS_ERROR_PMS_TYPE));
      break;
    }
}

const char *M5Client::Sensors::AQI::getFormattedAQI() {
  String formattedAqi;

  // "uuid;value_type:value;..."
  formattedAqi += uuid;
  formattedAqi += ";";
  formattedAqi += "pm10";
  formattedAqi += ":";
  formattedAqi.concat((float)(_pm10));
  formattedAqi += ";";
  formattedAqi += "pm25";
  formattedAqi += ":";
  formattedAqi.concat((float)(_pm25));
  formattedAqi += "/";
  formattedAqi += "temp";
  formattedAqi += ":";
  formattedAqi.concat((float)(_temp));
  formattedAqi += ";";
  formattedAqi += "hum";
  formattedAqi += ":";
  formattedAqi.concat((float)(_hum));

  auto res = formattedAqi.c_str();
  return res;
}

void M5Client::Sensors::AQI::AqLoop() {
    _hum = _sht20.readHumidity();
    _temp = _sht20.readTemperature();
    _pms->read();
    if (_pms) {
        _pm25 = _pms->pm25;
        _pm10 = _pms->pm10;
    } else
        M5Client::Sensors::AQI::handleError(*_pms);
}
