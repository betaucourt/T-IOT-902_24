#include "Communication/LoraProtocol.hpp"

long lastSendTime = 0;
long interval = 1000; // Delay 1s
const char* parseDataPM="";
const char* parseDataAQI="";

M5Client::Communication::LoraProtocol::LoraProtocol()
{
  this->frequency = 868E6;
  this->lora = LoRaClass();
}

M5Client::Communication::LoraProtocol::~LoraProtocol() {}

void M5Client::Communication::LoraProtocol::Init() {
  M5.begin();
  M5.Power.begin();
  Serial.begin(76800);

  while (!Serial);
  LoRa.setPins();
  if (!LoRa.begin(frequency)) { // initialize ratio at 868 MHz.Europe ratio
    Serial.println("LoRa init failed. Check your connections.");
    while (true);
  }
  lora.setSyncWord(0XF2);
  lora.setTxPower(2);
  Serial.println("LoRa init succeeded.");
}

char *M5Client::Communication::LoraProtocol::PostData(const char* data) {
  LoRa.beginPacket();
  LoRa.print(data);
  LoRa.endPacket();
}

void M5Client::Communication::LoraProtocol::Reinit() {
  Serial.println("LoRaReinitialization");
  LoRa.setPins();

  if (!LoRa.begin(frequency)) {
    Serial.println("LoRa init failed. Check your connections.");
    M5.Lcd.setCursor(0, 0);
    M5.Lcd.setTextColor(RED);
    M5.Lcd.println("Init failed!!!");
    while (true);
  }
  Serial.println("LoRa init succeeded.");
}

void M5Client::Communication::LoraProtocol::loopLoRa(const char* data) {
    if (millis() - lastSendTime > interval) {
    Serial.println(data);
    M5Client::Communication::LoraProtocol::PostData(data);
    lastSendTime = millis();
    interval = random(1000) + 500;
  }
  
  if(M5.BtnB.wasPressed()){
    M5.Lcd.print("REINIT");
    M5Client::Communication::LoraProtocol::Reinit();
  }
  M5.update();
}