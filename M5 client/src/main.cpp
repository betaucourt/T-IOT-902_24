#include <Arduino.h>
#include <M5Stack.h>
#include "../include/Communication/LoraProtocol.hpp"
#include "../include/Sensors/AQI.hpp"
#include "../include/sleep.hpp"

M5Client::Communication::LoraProtocol loraMain;
M5Client::Sensors::AQI aqSensor;
Sleep sleeping;

void setup() {
  loraMain.Init();
  aqSensor.AqiInit();
  sleeping.init();
}

void loop() {
  aqSensor.AqLoop();
  loraMain.loopLoRa(aqSensor.getFormattedAQI());
  sleeping.loop();
}