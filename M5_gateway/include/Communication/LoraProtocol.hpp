#ifndef LORA_HPP
#define LORA_HPP

#include <M5Stack.h>
#include <M5LoRa.h>

namespace M5Server {
  namespace Communication {
    class  LoraProtocol
    {
    public:
      LoraProtocol ();
      ~LoraProtocol();
      bool ListenAndReceiveData();
      const char* GetSensordata( );
      bool StartConnexion();
      bool StopConnexion();
      void Init() ;
      char* PostData(char * data) ;
      const char* getMessageToSend();
    private:
      float frequency;
      LoRaClass lora;
      const char * messageToSend;
    };
  }
}

#endif