#ifndef WIFI_HPP
#define WIFI_HPP

#include <M5Stack.h>
#include "WiFi.h"
#include <ArduinoJson.h>
#include <HTTPClient.h>
#include "Config.hpp"
#include <map>

typedef std::map<String, String> headers;
namespace M5Server {
  namespace Communication {
    class WifiProtocol{
      public:
        WifiProtocol();
        ~WifiProtocol();
        bool StartConnexion();
        bool SendMessage(String messsage, int pin);
        bool StopConnexion();
        void Init() ;
        char* PostData(char * data) ;
      private:
      const char * url;
      const char * serverPort;
      const char * ssid;
      const char * password;
    };
  }
}
#endif