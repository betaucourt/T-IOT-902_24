#ifndef PARSE_SENSOR_DATA_HPP
#define PARSE_SENSOR_DATA_HPP
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <Arduino.h>
#include <M5Stack.h>
#include <ArduinoJson.h>

namespace M5Server
{
  class ParseSensorsData
  {
  public:
    ParseSensorsData();
    ~ParseSensorsData();
    std::vector<std::vector<char*>> ParseData(const char *data);
    String FormatMessage(std::vector<char *> data);
    std::string Operation();
  };
}

#endif