#include <cstring>
#include <iostream>
#include "Communication/LoraProtocol.hpp"

byte localAddress = 0xBB; // address of this device
byte destination = 0xFF;  // destination to send to

M5Server::Communication::LoraProtocol::LoraProtocol() {
  this->frequency = 868E6;
  this->lora = LoRaClass();
}

M5Server::Communication::LoraProtocol::~LoraProtocol() {

}

bool M5Server::Communication::LoraProtocol::ListenAndReceiveData(){
  this->messageToSend = GetSensordata();
  if (messageToSend == NULL)
    return false;
  return true;
}

const char* M5Server::Communication::LoraProtocol::GetSensordata()
{
    int packetSize = LoRa.parsePacket();
    String messageToSend = "";
  if (packetSize) {
    while (LoRa.available()) {
      messageToSend.concat((char)LoRa.read());
    }
  }
  else
    return NULL;
  return messageToSend.c_str();
}

bool M5Server::Communication::LoraProtocol::StartConnexion()
{
  while (!Serial);
  Serial.println("LoRa receiver");
  if (!LoRa.begin(868E6))
  {
    Serial.println(".");
    while (true);
  }
  LoRa.setTxPower(2);
  Serial.println("LoRa init complete.");
  return true;
};
const char * M5Server::Communication::LoraProtocol::getMessageToSend() {
  return this->messageToSend;
}