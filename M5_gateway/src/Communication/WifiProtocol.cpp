#include "Communication/WifiProtocol.hpp"

M5Server::Communication::WifiProtocol::WifiProtocol(){
  this->url = SERVER_URL;
  this->serverPort = SERVER_PORT;
  this->ssid = WIFI_SSID;
  this->password = WIFI_PASSWORD;
}
M5Server::Communication::WifiProtocol::~WifiProtocol()
{}

bool M5Server::Communication::WifiProtocol::StartConnexion(){
    const int wifi_max_tries = 20;
  unsigned int tries = 0;

  WiFi.begin(this->ssid, this->password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.println("Connecting to WiFi..");
    Serial.println((int)WiFi.status());
    if (tries > wifi_max_tries)
    {
      Serial.println("Couldn't connect to WiFi: timeout");
      return false;
    }
    tries++;
  }
  Serial.println("Connected to WiFi");
  return true;
}
bool M5Server::Communication::WifiProtocol::SendMessage(String messsage , int pin){
  headers httpHeaders;
  httpHeaders["X-Sensor"] = "esp32-240AC4F9538C";
  httpHeaders["X-Pin"] = pin;
  httpHeaders["Content-Type"] = "application/json";
  String resp;
    if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;
    http.useHTTP10(true);
    http.begin(this->url);
    for (auto it = httpHeaders.begin(); it != httpHeaders.end(); it++)
    {
      http.addHeader(it->first.c_str(), it->second.c_str());
    }
    int httpCode = http.POST(messsage);
    if (httpCode > 0) {
        resp = http.getString();
        Serial.println(httpCode);
        Serial.println(resp);
    } else {
      Serial.println("Error on HTTP_POST request. Code: " + httpCode);
    }
    http.end();
  }
  return true;
}
bool M5Server::Communication::WifiProtocol::StopConnexion(){
  return true;
}