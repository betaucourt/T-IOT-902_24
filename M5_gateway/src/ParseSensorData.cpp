#include "ParseSensorData.hpp"



M5Server::ParseSensorsData::ParseSensorsData() {}

M5Server::ParseSensorsData::~ParseSensorsData() {}

std::vector<std::vector<char*>> M5Server::ParseSensorsData::ParseData(const char *data) {
   char* str = strdup(data);
    std::vector<std::vector<char*>> value;
    std::vector<char *> first;
    std::vector<char *> second;
    char *tmp ;
    char * firstStr;
    char * secondStr;
    tmp = strtok(str,";:");
    if (tmp == "grp25")
      return value;
    firstStr = strtok(NULL,"/");
    secondStr = strtok(NULL,"/");
    tmp = strtok(firstStr,";:");
    first.push_back(tmp);
     while (tmp != NULL)
      {
        tmp = strtok(NULL, ";:");
        if (tmp != NULL)
          first.push_back(tmp);
     }
    tmp = strtok(secondStr,";:");
    second.push_back(tmp);
      while (tmp != NULL)
      {
        tmp = strtok(NULL, ";:");
        if (tmp != NULL)
          second.push_back(tmp);
     }
     value.push_back(first);
     value.push_back(second);
  return value;
}

String  M5Server::ParseSensorsData::FormatMessage(std::vector<char *> data) {
  String payload;
  StaticJsonDocument<2048> jsonPayload;
  jsonPayload["software_version"] = "1.1.0";
  int t = 0;
  for (int i = 0; i < data.size() ; i+=2)
  {
    Serial.print("parse: ");
    Serial.println(data[i]);
    if (strcmp(data[i] , "pm10") == 0)
      {
      jsonPayload["sensordatavalues"][t]["value_type"]= "P1";
      jsonPayload["sensordatavalues"][t]["value"]= data[i+1];
      t++;
      }
    else if (strcmp(data[i], "pm25") == 0)
      {
      jsonPayload["sensordatavalues"][t]["value_type"]= "P2";
      jsonPayload["sensordatavalues"][t]["value"]= data[i+1];
      t++;
      }
    else if (strcmp(data[i], "temp") == 0)
      {
      jsonPayload["sensordatavalues"][t]["value_type"]= "temperature";
      jsonPayload["sensordatavalues"][t]["value"]= data[i+1];
      t++;
      }
    else if (strcmp(data[i], "hum") == 0 )
      {
      jsonPayload["sensordatavalues"][t]["value_type"]= "humidity";
      jsonPayload["sensordatavalues"][t]["value"]= data[i+1];
      t++;
      }

  }
  serializeJson(jsonPayload, payload);
  return payload;
}

std::string M5Server::ParseSensorsData::Operation() {}
