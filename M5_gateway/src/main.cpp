#include <Arduino.h>
#include <M5Stack.h>
#include "Communication/LoraProtocol.hpp"
#include "Communication/WifiProtocol.hpp"
#include "ParseSensorData.hpp"

M5Server::Communication::LoraProtocol lora;
M5Server::ParseSensorsData parser;
M5Server::Communication::WifiProtocol wifi;

void setup() {
  M5.begin();
  M5.Power.begin();
  Serial.begin(76800);
  wifi.StartConnexion();
  lora.StartConnexion();
}

void loop() {
  
  std::vector<std::vector<char *>> messageParse;
  if (lora.ListenAndReceiveData())
  {
    const char * message = lora.getMessageToSend();
    if (message != NULL)
    {
      messageParse = parser.ParseData(message);
      if (messageParse.size() != 0) {
        Serial.println("message parsée");
         for (int i = 0; i < messageParse[0].size(); i++){
           Serial.println(messageParse[0][i]);
          }

         for (int i = 0; i < messageParse[1].size(); i++){
            Serial.println(messageParse[1][i]);
          }
        for (int i = 0;i < messageParse.size(); i++)
        {
          int pin;
          if (messageParse[i][0]) {
            strcmp(messageParse[i][0] , "pm10") == 0 || strcmp( messageParse[i][0], "pm25") == 0 ?  pin = 1 : pin = 7;
            Serial.println(pin);
            String dataFormated = parser.FormatMessage(messageParse[i]);
            Serial.print(dataFormated);
            wifi.SendMessage(dataFormated, pin);
          }
         }
        
      }
    }

  }
}